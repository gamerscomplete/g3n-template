package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gamerscomplete/application"
)

func main() {
	var flagLevel string
	flag.StringVar(&flagLevel, "level", "warn", "logging level")
	flag.Parse()

	level, err := log.ParseLevel(flagLevel)
	if err != nil {
		log.Warn("failed to parse log level from flag. defaulting to warn")
		log.SetLevel(log.WarnLevel)
	} else {
		log.SetLevel(level)
	}

	//This allows colors to show up in the logs
	log.SetFormatter(&log.TextFormatter{
		ForceColors: true,
	})

	//Create a new application with the window title set to "template" and a targetFPS of 60
	app := application.CreateApp("template", 60)

	//Run forever in a loop calling run which triggers update
	for {
		app.Run()
	}
}
